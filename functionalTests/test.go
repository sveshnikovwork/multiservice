package main

import (
	"bitbucket.org/sveshnikovwork/multiservice/v3"
	log "github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

func main() {
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(new(prefixed.TextFormatter))

	service := multiservice.MultiService(
		map[string]multiservice.ServiceInterface{
			"Test-Service": &service{},
		},
		39000,
	)

	log.Warning(service.Run())
	log.Print(`All stopped`)
}
