package main

import (
	"time"
)

// Тестовая служба
type service struct {
	isStarted bool
	isAlive   bool
}

// Возвращает статус состояния запуска сервиса
func (s *service) IsStarted() bool {
	return s.isStarted
}

// Возвращает статус сервиса: живой или нет
func (s *service) IsAlive() bool {
	return s.isAlive
}

// Запуск сервиса
func (s *service) Run() error {
	s.isStarted = true
	s.isAlive = true
	time.Sleep(time.Second * 2)

	s.isAlive = false
	return nil
}

// Правильная остановка сервиса
func (s *service) GracefulShutdown() {
	time.Sleep(time.Millisecond * 500)
}
