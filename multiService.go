package multiservice

import (
	"bitbucket.org/sveshnikovwork/multiservice/v3/helpers"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/xlab/closer"
	"sync"
	"time"
)

// Сервис с множественными подсервисами
type multiService struct {
	services map[string]ServiceInterface
	logger   *logrus.Entry
}

// Проверяет, запущен ли сервер
func (m *multiService) IsServicesRun() bool {
	status := true
	for _, service := range m.services {
		status = status && service.IsStarted()
	}

	return status
}

// Проверяет, остановлен ли хоть один из сервисов
func (m *multiService) IsServicesAlive() bool {
	status := true
	for _, service := range m.services {
		status = status && service.IsAlive()
	}

	return status
}

// Запуск основного сервиса
func (m *multiService) Run() error {
	m.logger.WithFields(logrus.Fields{
		"code": 20000,
	}).Info(fmt.Sprintf("Initializing multiservice for %v services", len(m.services)))

	// Каналы для передачи сообщений об ошибке и об остановке
	servicesStopChan := make(chan string, len(m.services))
	servicesErrorChan := make(chan error, len(m.services))
	defer close(servicesStopChan)
	defer close(servicesErrorChan)

	// Контекст и функция остановки сервисов
	ctx, cancel := context.WithCancel(context.Background())

	// Регистрируем прослушку события остановки приложения по системному коду завершения
	closer.Bind(func() {
		m.cleanUp(cancel)
	})

	m.logger.WithFields(logrus.Fields{
		"code": 20001,
	}).Debug("Graceful shutdown listening starts")

	// Отправляем на запуск все сервисы
	startedServices := map[string]bool{}
	for name, service := range m.services {
		startedServices[name] = true
		go m.startService(ctx, name, service, servicesStopChan, servicesErrorChan)
	}

	// Дожидаемся окончания работы сервисов и отправляем результат работы
	return m.listenServiceStop(ctx, cancel, servicesStopChan, servicesErrorChan, startedServices)
}

// Запуск сервиса
func (m *multiService) startService(
	ctx context.Context,
	name string,
	service ServiceInterface,
	servicesStopChan chan string,
	servicesErrorChan chan error,
) {
	m.logger.WithFields(logrus.Fields{
		"code":    20002,
		"service": name,
	}).Info("Starting service")

	err := service.Run()

	// Если завершение работы пришло с самого верха, значит нет нужды отправлять статистику
	// в общий канал. Нужно просто остановить работу сервиса
	select {
	case <-ctx.Done():
		return
	default:
	}

	if nil == err {
		servicesStopChan <- name
		return
	}

	m.logger.WithFields(logrus.Fields{
		"code":    50000,
		"service": name,
		"err":     err.Error(),
	}).Error("Response error from service")

	servicesErrorChan <- err
}

// Обработка остановки сервисов
func (m *multiService) listenServiceStop(
	ctx context.Context,
	cancel context.CancelFunc,
	servicesStopChan chan string,
	servicesErrorChan chan error,
	startedServices map[string]bool,
) (err error) {
	for {
		select {
		case name := <-servicesStopChan:
			m.logger.WithFields(logrus.Fields{
				"code":    20200,
				"service": name,
			}).Info("Service complete")

			stoppedServices := 0
			startedServices[name] = false
			for _, status := range startedServices {
				if false == status {
					stoppedServices += 1
				}
			}

			// Все сервисы остановлены
			if stoppedServices == len(startedServices) {
				m.logger.WithFields(logrus.Fields{
					"code": 20010,
				}).Warning("All services complete")

				cancel()
			}
			break
		case err = <-servicesErrorChan:
			cancel()
			break
		case _ = <-ctx.Done():
			m.gracefulShutdown()

			return
		}
	}
}

// Запуск основного сервиса
func (m *multiService) gracefulShutdown() {
	m.logger.WithFields(logrus.Fields{
		"code": 20004,
	}).Info("Initialized graceful shutdown")

	var wg sync.WaitGroup
	wg.Add(len(m.services))

	for name, service := range m.services {
		go func(name string, service ServiceInterface, wg *sync.WaitGroup) {
			defer wg.Done()

			m.logger.WithFields(logrus.Fields{
				"code":    20004,
				"service": name,
			}).Debug(`Shutdown service`)

			service.GracefulShutdown()
		}(name, service, &wg)
	}

	wg.Wait()

	m.logger.WithFields(logrus.Fields{
		"code": 20006,
	}).Info(`Services gracefully shutdown`)
}

// Подписка на сигнал остановки приложения
func (m *multiService) cleanUp(cancel context.CancelFunc) {
	m.logger.WithFields(logrus.Fields{
		"code": 20003,
	}).Info("Response system terminate signal")

	cancel()

	// Ожидание для завершения основного потока, т.к. в нем может еще что-то выполняться
	time.Sleep(10 * time.Hour)
}

// Конструктор сервиса
func MultiService(
	services map[string]ServiceInterface,
	livenessPort uint16,
) MultiServiceInterface {
	mainService := &multiService{
		logger: helpers.NewLogger(`MultiService`),
	}

	if livenessPort > 0 {
		livenessService := newLivenessService(livenessPort)
		livenessService.SetMainService(mainService)

		services[`LivenessService`] = livenessService
	}

	mainService.services = services

	return mainService
}
