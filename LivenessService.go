package multiservice

import (
	"bitbucket.org/sveshnikovwork/multiservice/v3/helpers"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
)

// Сервис проверки liveness probe для kubernetes
type livenessService struct {
	mainService  MultiServiceInterface
	logger       *logrus.Entry
	isStarted    bool
	isAlive      bool
	server       *fasthttp.Server
	livenessPort uint16
}

// Возвращает статус состояния запуска сервиса
func (l *livenessService) IsStarted() bool {
	return l.isStarted
}

// Возвращает статус сервиса: живой или нет
func (l *livenessService) IsAlive() bool {
	return l.isAlive
}

// Запуск сервиса
func (l *livenessService) Run() error {
	server := fmt.Sprintf(`0.0.0.0:%v`, l.livenessPort)
	l.isStarted = true
	l.logger.WithFields(logrus.Fields{
		"code":   2001,
		"server": server,
	}).Debug(`Liveness probe service starts`)

	fasthttpServer := &fasthttp.Server{
		Handler: func(ctx *fasthttp.RequestCtx) {
			ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
			ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
			ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")

			status := 200
			message := `Ok`

			switch string(ctx.Path()) {
			case "/ready":
				if !l.mainService.IsServicesRun() {
					status = 500
					message = `Is not ready`
				}
			case "/alive":
				if !l.mainService.IsServicesAlive() {
					status = 500
					message = `Is not alive`
				}
			default:
				status = 404
				message = `Not found`
			}

			if status != 200 {
				l.logger.WithFields(logrus.Fields{
					"code":            400,
					"url":             string(ctx.Path()),
					"statusCode":      status,
					"responseMessage": message,
				}).Warning(`Liveness probe failed`)
			} else {
				l.logger.WithFields(logrus.Fields{
					"code":            2002,
					"url":             string(ctx.Path()),
					"statusCode":      status,
					"responseMessage": message,
				}).Debug(`Received liveness probe request`)
			}

			ctx.SetStatusCode(status)
			fmt.Fprintf(ctx, `%v`, message)
		},
	}
	l.server = fasthttpServer
	err := fasthttpServer.ListenAndServe(server)

	l.isAlive = false
	return err
}

// Правильная остановка сервиса
func (l *livenessService) GracefulShutdown() {
	l.logger.WithFields(logrus.Fields{
		"code": 1001,
	}).Debug(`Starting terminating of LivenessProbe`)

	_ = l.server.Shutdown()

	l.logger.WithFields(logrus.Fields{
		"code": 1002,
	}).Debug(`LivenessProbe server gracefully stopped`)
}

// Подключение основного сервиса
func (l *livenessService) SetMainService(mainService MultiServiceInterface) {
	l.mainService = mainService
}

// Конструктор сервиса
func newLivenessService(livenessPort uint16) *livenessService {
	return &livenessService{
		mainService:  nil,
		logger:       helpers.NewLogger(`LivenessService`),
		isStarted:    false,
		isAlive:      true,
		livenessPort: livenessPort,
	}
}
