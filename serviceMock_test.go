package multiservice

import (
	"time"
)

// Подставка для тестирования библиотеки
type serviceMock struct {
	RunIsCalled      bool
	ShutdownIsCalled bool
	RunResult        error
}

// Возвращает статус состояния запуска сервиса
func (s *serviceMock) IsStarted() bool {
	return true
}

// Возвращает статус сервиса: живой или нет
func (s *serviceMock) IsAlive() bool {
	return true
}

// Запуск сервиса
func (s *serviceMock) Run() error {
	s.RunIsCalled = true

	time.Sleep(time.Millisecond * 100)

	return s.RunResult
}

// Правильная остановка сервиса
func (s *serviceMock) GracefulShutdown() {
	s.ShutdownIsCalled = true

	time.Sleep(time.Millisecond * 100)
}
