package multiservice

import (
	"bitbucket.org/sveshnikovwork/multiservice/v3/helpers"
	"errors"
	"github.com/sirupsen/logrus"
	"testing"
)

// Тестирование запуска сервисов
func Test_multiService_Run_ErrorReturns(t *testing.T) {
	type fields struct {
		services map[string]ServiceInterface
		logger   *logrus.Entry
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "Тестирование отсутствия ошибки, если сервис ее не возвращает",
			fields: fields{
				services: map[string]ServiceInterface{
					"test-1": &serviceMock{
						RunIsCalled:      false,
						ShutdownIsCalled: false,
						RunResult:        nil,
					},
				},
				logger: helpers.NewLogger(`test`),
			},
			wantErr: false,
		},
		{
			name: "Тестирование наличия ошибки, если сервис ее возвращает",
			fields: fields{
				services: map[string]ServiceInterface{
					"test-1": &serviceMock{
						RunIsCalled:      false,
						ShutdownIsCalled: false,
						RunResult:        errors.New("Test error"),
					},
				},
				logger: helpers.NewLogger(`test`),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := multiService{
				services: tt.fields.services,
				logger:   tt.fields.logger,
			}
			if err := m.Run(); (err != nil) != tt.wantErr {
				t.Errorf("Run() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

// Тестирование запуска подсервисов
func Test_multiService_Run_Start(t *testing.T) {
	type fields struct {
		services map[string]ServiceInterface
		logger   *logrus.Entry
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "Тестирование запуска всех сервисов. 2 сервиса.",
			fields: fields{
				services: map[string]ServiceInterface{
					"test-1": &serviceMock{RunIsCalled: false},
					"test-2": &serviceMock{RunIsCalled: false},
				},
				logger: helpers.NewLogger(`test`),
			},
		},
		{
			name: "Тестирование запуска всех сервисов. 10 сервисов.",
			fields: fields{
				services: map[string]ServiceInterface{
					"test-1": &serviceMock{RunIsCalled: false},
					"test-2": &serviceMock{RunIsCalled: false},
					"test-3": &serviceMock{RunIsCalled: false},
					"test-4": &serviceMock{RunIsCalled: false},
					"test-5": &serviceMock{RunIsCalled: false},
					"test-6": &serviceMock{RunIsCalled: false},
					"test-7": &serviceMock{RunIsCalled: false},
					"test-8": &serviceMock{RunIsCalled: false},
					"test-9": &serviceMock{RunIsCalled: false},
					"test-0": &serviceMock{RunIsCalled: false},
				},
				logger: helpers.NewLogger(`test`),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := multiService{
				services: tt.fields.services,
				logger:   tt.fields.logger,
			}

			_ = m.Run()

			for name, service := range tt.fields.services {
				service, _ := service.(*serviceMock)
				if true != service.RunIsCalled {
					t.Errorf("Run() service %v is not started", name)
				}
			}
		})
	}
}

// Тестирование остановки подсервисов
func Test_multiService_Run_Stop(t *testing.T) {
	type fields struct {
		services map[string]ServiceInterface
		logger   *logrus.Entry
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "Тестирование остановки всех сервисов. 2 сервиса.",
			fields: fields{
				services: map[string]ServiceInterface{
					"test-1": &serviceMock{ShutdownIsCalled: false},
					"test-2": &serviceMock{ShutdownIsCalled: false},
				},
				logger: helpers.NewLogger(`test`),
			},
		},
		{
			name: "Тестирование остановки всех сервисов. 8 сервисов.",
			fields: fields{
				services: map[string]ServiceInterface{
					"test-1": &serviceMock{ShutdownIsCalled: false},
					"test-2": &serviceMock{ShutdownIsCalled: false},
					"test-3": &serviceMock{ShutdownIsCalled: false},
					"test-4": &serviceMock{ShutdownIsCalled: false},
					"test-5": &serviceMock{ShutdownIsCalled: false},
					"test-6": &serviceMock{ShutdownIsCalled: false},
					"test-7": &serviceMock{ShutdownIsCalled: false},
					"test-8": &serviceMock{ShutdownIsCalled: false},
				},
				logger: helpers.NewLogger(`test`),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := multiService{
				services: tt.fields.services,
				logger:   tt.fields.logger,
			}

			_ = m.Run()

			for name, service := range tt.fields.services {
				service, _ := service.(*serviceMock)
				if true != service.ShutdownIsCalled {
					t.Errorf("Run() service %v is not stopped", name)
				}
			}
		})
	}
}
