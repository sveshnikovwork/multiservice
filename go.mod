module bitbucket.org/sveshnikovwork/multiservice/v3

go 1.14

require (
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.4 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/valyala/fasthttp v1.15.1
	github.com/x-cray/logrus-prefixed-formatter v0.5.2
	github.com/xlab/closer v0.0.0-20190328110542-03326addb7c2
)
