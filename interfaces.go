package multiservice

// Интерфейс сервиса с множественными подсервисами
type MultiServiceInterface interface {
	// Запуск основного сервиса
	Run() error

	// Проверяет, запущен ли сервер
	IsServicesRun() bool

	// Проверяет, остановлен ли хоть один из сервисов
	IsServicesAlive() bool
}

// Интерфейс сервиса, для параллельного запуска.
type ServiceInterface interface {
	// Запуск сервиса
	Run() error

	// Правильная остановка сервиса
	GracefulShutdown()

	// Возвращает статус состояния запуска сервиса
	IsStarted() bool

	// Возвращает статус сервиса: живой или нет
	IsAlive() bool
}

// Тип, описывающий фабрику службы
type TMultiServiceFactory = func(services map[string]ServiceInterface) MultiServiceInterface
